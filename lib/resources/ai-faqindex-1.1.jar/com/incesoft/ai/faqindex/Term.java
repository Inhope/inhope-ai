package com.incesoft.ai.faqindex;

import java.util.Arrays;

import com.incesoft.ai.faqindex.support.TermHolder;

public class Term {
	
	public static final byte TYPE_WORD = 0;
	public static final byte TYPE_SYNONYM = 1;
	public static final byte TYPE_KEY_WORD = 2;
	public static final byte TYPE_KEY_SYNONYM = 3;
	public static final byte TYPE_TEMPLATE_WORD = 4;
	public static final byte TYPE_KEY_TEMPLATE_WORD = 5;
	public static final byte TYPE_PUNCTUATION = 6;
	public static final byte TYPE_STOPWORD = 7;
	public static final byte TYPE_ULTRA_TEMPLATE_WORD = 8;
	public static final byte TYPE_FLEX_KEY_TEMPLATE_WORD = 9;
	public static final byte TYPE_FLEX_ULTRA_TEMPLATE_WORD = 10;
	public static final byte TYPE_ENTITY = 11;
	
	public String word = null;
	public String[] wordclasses = null;
	public byte type = TYPE_WORD;
	
	public Term(){}
	public Term(String word) {
		this.word = word;
	}
	public Term(String word,String[] wordclasses) {
		this.word = word;
		this.wordclasses = wordclasses;
	}
	public Term(String word,String[] wordclasses, byte type) {
		this.word = word;
		this.wordclasses = wordclasses;
		this.type = type;
	}
	
	public boolean isSynonym() {
		return type == TYPE_SYNONYM || type == TYPE_KEY_SYNONYM || (type<40 && type>30) || (type<=-30 && type>-40);
	}
	
	public boolean isKeyWord() {
		return (type == TYPE_KEY_WORD) || (type<30 && type>20);
	}
	public int getKeyLevel() {
		if(type<40 && type>30) 
		return type - 30;
		if(type<30 && type>20) 
		return type - 20;
		return 0;
	}
	public void setKeyWordLevel(int level) {
		if(level < 1 || level > 19) throw new IllegalArgumentException();
		if(type != TYPE_KEY_WORD) throw new IllegalStateException();
		if(level<10)
			this.type = (byte)(20 + level);
		else
			this.type = (byte)(-10 - level);
	}
	public int getKeyWordLevel() {
		if(type<21 || type>29) return 0;
		return type - 20;
	}
	public boolean isKeySynonym() {
		return (type == TYPE_KEY_SYNONYM) || (type<40 && type>30);
	}
	public void setKeySynonymLevel(int level) {
		if(level < 1 || level > 19) throw new IllegalArgumentException();
		if(type != TYPE_KEY_SYNONYM) throw new IllegalStateException();
		if(level<10)
			this.type = (byte)(30 + level);
		else
			this.type = (byte)(-20 - level);
	}
	public int getKeySynonymLevel() {
		if(type<31 || type>39) return 0;
		return type - 30;
	}
	
	public boolean isTemplateWord() {
		return this.type==Term.TYPE_TEMPLATE_WORD || this.type==Term.TYPE_KEY_TEMPLATE_WORD || (this.type>=8 && this.type<=10);
	}
	public boolean isStrongTemplateWord() {
		return this.type==Term.TYPE_KEY_TEMPLATE_WORD || (this.type>=8 && this.type<=10);
	}
	public boolean isFlexTemplateWord() {
		return this.type==Term.TYPE_FLEX_KEY_TEMPLATE_WORD || this.type==Term.TYPE_FLEX_ULTRA_TEMPLATE_WORD;
	}
	
	public String toString() {
		return word + "(" + Arrays.toString(wordclasses) + ")";
	}
	
	private static final String typeNames[] = new String[]{
		"word","synonym","keyword","keysynonym","templateword","keytemplateword","punctuation","stopword","ultratemplateword","flexkeytemplateword","flexultratemplateword","entity"
	};
	public static String typeToString(byte type) {
		if(type<40 && type>30) 
			return "keysynonym" + (type-30);
		else if(type<30 && type>20) 
			return "keyword" + (type-20);
		else if(type<=-30 && type>-40)
			return "weaksynonym" + (-type-30);
		else if(type<=-20 && type>-30)
			return "weakword" + (-type-20);
		else if(type<0)
			return String.valueOf(type);
		return typeNames[type];
	}
	private static final byte levels[] = new byte[]{
		2/*nonsenseWords2*/,2/*nonsenseWords1*/,
		3/*word*/,3/*synonym*/,4/*keyword*/,4/*keysynonym*/,3/*templateword*/,4/*keytemplateword*/,
		1/*punctuation*/,1/*stopword*/,4/*ultratemplateword*/,4/*flexkeytemplateword*/,4/*flexultratemplateword*/,3/*entity*/
	};
	public static int level(byte type) {
		if(type<40 && type>20) 
			return 5;
		else if(type<=-20)
			return 3;
		return levels[type+2];
	}
	
//	public static void main(String[] args) throws Exception {
//		long old = System.currentTimeMillis();
//		Term term = new Term("a",new String[]{"a"},(byte)9);
//		TermHolder holder = new TermHolder(term);
//		int b = 0;
//		for(int i=0;i<80000000;i++) {
//			holder.term.type++;
//			if(holder.term.isTemplateWord()) {
//				b++;
//			}
//		}
//		
//		
//	}
}
