package com.incesoft.ai.faqindex;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class AILog implements Log {
	
	static ThreadLocal<StringBuilder> ctx = new ThreadLocal<StringBuilder>();
	public static void setReport(StringBuilder report) {
		ctx.set(report);
	}
	public static StringBuilder getReport() {
		return ctx.get();
	}
	
	@SuppressWarnings("rawtypes")
	public static AILog getLog(Class clazz) {
		AILog ailog = new AILog(LogFactory.getLog(clazz));
		return ailog;
	}
	public static AILog getLog(String name) {
		AILog ailog = new AILog(LogFactory.getLog(name));
		return ailog;
	}

	private Log log;
	
	private AILog(Log log) {
		this.log = log;
	}
	
	public boolean isDebugEnabled() {
		if(AILog.getReport() != null)return true;
		return log.isDebugEnabled();
	}
	public boolean isInfoEnabled() {
		if(AILog.getReport() != null)return true;
		return log.isInfoEnabled();
	}
	public void debug(Object message) {
		StringBuilder report = AILog.getReport();
		if(report != null)
			report.append(message + "\r\n");
		log.debug(message);
	}
	public void debug(Object message, Throwable t) {
		StringBuilder report = AILog.getReport();
		if(report != null)
			report.append(message + "\r\n");
		log.debug(message, t);
	}
	public void info(Object message) {
		StringBuilder report = AILog.getReport();
		if(report != null)
			report.append(message + "\r\n");
		log.info(message);
	}
	public void info(Object message, Throwable t) {
		StringBuilder report = AILog.getReport();
		if(report != null)
			report.append(message + "\r\n");
		log.info(message, t);
	}
	
	
	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}
	public boolean isWarnEnabled() {
		return log.isWarnEnabled();
	}
	public boolean isErrorEnabled() {
		return log.isErrorEnabled();
	}
	public boolean isFatalEnabled() {
		return log.isFatalEnabled();
	}

	public void trace(Object message) {
		log.trace(message);
	}
	public void trace(Object message, Throwable t) {
		log.trace(message, t);
	}
	public void warn(Object message) {
		log.warn(message);
	}
	public void warn(Object message, Throwable t) {
		log.warn(message, t);
	}
	public void error(Object message) {
		log.error(message);
	}
	public void error(Object message, Throwable t) {
		log.error(message, t);
	}
	public void fatal(Object message) {
		log.fatal(message);
	}
	public void fatal(Object message, Throwable t) {
		log.fatal(message, t);
	}

}
