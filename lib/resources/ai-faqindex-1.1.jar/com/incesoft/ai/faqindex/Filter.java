package com.incesoft.ai.faqindex;

public interface Filter<T> {

	public int filter(T doc);
	
	public int rank(T doc);
	
	public boolean end();
}
