package com.incesoft.ai.faqindex;

import java.io.File;
import java.util.List;

import com.incesoft.ai.faqindex.support.FAQIndexImpl;


public abstract class FAQIndex<T> {
	
	private static ThreadLocal<FAQIndex<String>> wordSemanticBase = new ThreadLocal<FAQIndex<String>>();
	
	public static void setWordSemanticBase(FAQIndex<String> base) {
		wordSemanticBase.set(base);
	}
	public static FAQIndex<String> getWordSemanticBase() {
		return wordSemanticBase.get();
	}
	
	public static <T> FAQIndex<T> open(Class<T> docClass, File indexDir) throws Exception {
		return new FAQIndexImpl<T>(docClass,indexDir);
	}
	
	public abstract void close() throws Exception;
	
	public abstract void clear() throws Exception;

	public abstract void saveDocument(String docId, T document) throws Exception;
	
	public abstract void deleteDocument(String docId) throws Exception;
	
	public abstract void feedDocument(String docId, String feedId, List<Term> feed) throws Exception;
	
	public abstract void deleteFeed(String docId, String feedId) throws Exception;
	
	public abstract ScoreDoc<T>[] search(List<Term> terms, int top) throws Exception;
	
	public abstract ScoreDoc<T>[] search(List<Term> terms, int top, float threshold, Filter<T> filter) throws Exception;
	
	public abstract ScoreDoc<T>[] search(List<Term> terms, int top, float threshold, float searchThreshold, Filter<T> filter) throws Exception;
	
	public abstract ScoreDoc<T>[] search(List<Term> terms, int top, float threshold, float searchThreshold, Filter<T> filter, int mode) throws Exception;
	
	public abstract T getDocument(String docId);
	
	public abstract List<Term> getFeed(String docId, String feedId);
	
	public abstract int getTotalWords();
	public abstract int getTotalTerms();
	
	public abstract List<String> getSearchExKey(String wordclass);
	
	public abstract int getDocFreq(String word);
	public abstract int getTotalDocs();
	public abstract int getTotalNodes();

	public abstract int getTotalFeeds();
	
	public abstract int getTermFreq(String docId, String word);
	public abstract int getTotalTerms(String docId);
	
	public abstract String getOptimalDoc(String word);
	
	public abstract double getWeight(String word);
	public static double getStaticWeight(String word) {
		return FAQIndexImpl.getStaticWeight(word);
	}
	
	public abstract int getScoreVersion();
	
	public abstract long getLastModified();
	
	public abstract double similarity(List<Term> terms1, List<Term> terms2);
	
}
