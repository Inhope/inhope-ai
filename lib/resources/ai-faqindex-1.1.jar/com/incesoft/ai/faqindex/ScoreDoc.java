package com.incesoft.ai.faqindex;

import com.incesoft.ai.faqindex.support.TermHolder;


public class ScoreDoc<T> {
	public double score;
	public String docId;
	public String feedId;
	public TermHolder[] feed;
	public T doc;
	public int rank = Integer.MAX_VALUE;
}
