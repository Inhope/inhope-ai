package com.incesoft.ai.faqindex;

import java.util.Arrays;

public class GroupTerm extends Term {
	public byte groupId;
	
	public GroupTerm(){}
	
	public GroupTerm(int groupId) {
		this.groupId = (byte)groupId;
	}
	
	public GroupTerm(int groupId, Term term) {
		this.groupId = (byte)groupId;
		this.type = term.type;
		this.word = term.word;
		this.wordclasses = term.wordclasses;
	}
	
	public String toString() {
		return word + "(" + Arrays.toString(wordclasses) + ")" + "{" + groupId + "}";
	}
}
