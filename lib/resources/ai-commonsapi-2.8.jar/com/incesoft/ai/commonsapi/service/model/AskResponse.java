
package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;

/**
 * 问答输出类型
 * @author Boyce Lee
 */
public abstract class AskResponse implements Serializable {

	private WordInfo[] segmentResult;

	public WordInfo[] getSegmentResult() {
		return segmentResult;
	}

	public void setSegmentResult(WordInfo[] segmentResult) {
		this.segmentResult=segmentResult;
	}
}
