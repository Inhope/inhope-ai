package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

public class FuzzyResponse extends AskResponse{
	
	private static final long serialVersionUID = 1L;
	
	public final static short TYPE_USERTERM=0;
	public final static short TYPE_POPTERM=1;

	private String moduleId;
	
	private short termType;//搜索关键词类型（即用户词汇和热门词汇）㣩
	
	private String[] targetNodeIDs;
	
	private String[] targetQuestions;
	
	private String[] matchedQuestions;
	
	private float[] similarities;
	
	private String[] targetAnswers;

	
	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String[] getTargetNodeIDs() {
		return targetNodeIDs;
	}

	public void setTargetNodeIDs(String[] targetNodeIDs) {
		this.targetNodeIDs = targetNodeIDs;
	}

	public String[] getTargetQuestions() {
		return targetQuestions;
	}

	public void setTargetQuestions(String[] targetQuestions) {
		this.targetQuestions = targetQuestions;
	}

	public String[] getTargetAnswers() {
		return targetAnswers;
	}

	public void setTargetAnswers(String[] targetAnswers) {
		this.targetAnswers = targetAnswers;
	}

	public short getTermType() {
		return termType;
	}

	public void setTermType(short termType) {
		this.termType = termType;
	}

	public float[] getSimilarities() {
		return similarities;
	}

	public void setSimilarities(float[] similarities) {
		this.similarities = similarities;
	}

	public String[] getMatchedQuestions() {
		return matchedQuestions;
	}

	public void setMatchedQuestions(String[] matchedQuestions) {
		this.matchedQuestions = matchedQuestions;
	}
}
