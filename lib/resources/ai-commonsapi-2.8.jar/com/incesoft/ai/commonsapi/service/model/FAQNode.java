package com.incesoft.ai.commonsapi.service.model;

/**
 * FAQ库节点类型
 * @author Boyce Lee
 */
public class FAQNode extends Node {
	
	private static final long serialVersionUID = 1L;
	
	private long expiration;

	/**
	 * 获取节点过期时间
	 * @return 节点过期时间
	 */
	public long getExpiration() {
		return expiration;
	}
	/**
	 * 设置节点过期时间
	 * @param expiration 节点过期时间
	 */
	public void setExpiration(long expiration) {
		this.expiration = expiration;
	}
	
	public FAQNode clone() {
		FAQNode node = new FAQNode();
		node.setNodeId(nodeId);
		node.setTags(tags);
		node.setAttachment(attachment);
		node.setExpiration(expiration);
		if(question != null) node.setQuestion(question.clone());
		if(answer != null) node.setAnswer(answer.clone());
		return node;
	}
}
