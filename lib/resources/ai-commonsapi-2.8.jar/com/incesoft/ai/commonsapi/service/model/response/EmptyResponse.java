package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

/**
 * 空响应，表示知识库没有对应的问题与目标问句匹配
 * @author Boyce Lee
 */
public class EmptyResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;
}
