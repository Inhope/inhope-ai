package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;

/**
 * 问答输入类型
 * 其中问题类别与Question类型中的类别相关联
 * 问题类别的概念广义上说应该为搜索时的过滤因素
 * @see Question
 * @author Boyce Lee
 */
public class AskRequest implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static final short RENDER_TYPE_SEGMENT_DISABLED = -3;
	public static final short RENDER_TYPE_SEGMENT_RESULT = -2;
	public static final short RENDER_TYPE_DEBUG = -1;
	public static final short RENDER_TYPE_RAW = 0;
	public static final short RENDER_TYPE_LIST0 = 1;
	public static final short RENDER_TYPE_LIST1 = 2;
	public static final short RENDER_TYPE_CLASSATTR = 3;
	public static final short RENDER_TYPE_NODE_ID = 4;
	
	private String userId;
	private String question;
	private String[] tags;
	private String[] modules;
	private Map<String,String> filter;
	private short renderType;
	private short maxReturn;
	private boolean wordInfoRequired;

	/**
	 * 获取用户ID
	 * @return 用户ID
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * 设置用户ID
	 * @param userId 用户ID
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * 获取问题内容
	 * @return 问题内容
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * 设置问题内容
	 * @param question 问题内容
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * 获取知识点检索标签
	 * @return 知识点检索标签
	 */
	public String[] getTags() {
		return tags;
	}
	/**
	 * 设置知识点检索标签
	 * @param tags 知识点检索标签
	 */
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	public short getMaxReturn() {
		return maxReturn;
	}
	public void setMaxReturn(short maxReturn) {
		this.maxReturn = maxReturn;
	}
	public short getRenderType() {
		return renderType;
	}
	public void setRenderType(short renderType) {
		this.renderType = renderType;
	}
	public String[] getModules() {
		return modules;
	}
	public void setModules(String[] modules) {
		this.modules = modules;
	}
	public Map<String, String> getFilter() {
		return filter;
	}
	public void setFilter(Map<String, String> filter) {
		this.filter = filter;
	}
	public boolean isWordInfoRequired() {
		return wordInfoRequired;
	}
	public void setWordInfoRequired(boolean wordInfoRequired) {
		this.wordInfoRequired = wordInfoRequired;
	}
}
