package com.incesoft.ai.commonsapi.service.model.response;

import java.util.List;
import java.util.Map;

import com.incesoft.ai.commonsapi.service.model.AskResponse;
import com.incesoft.ai.commonsapi.service.model.SuggestedQA;

public class SuggestedResponse extends AskResponse {
	
	public static final int TYPE_SIMILAR = 0;
	public static final int TYPE_GROUP = 1;
	public static final int TYPE_COMPARE = 2;
	public static final int TYPE_RECOMMEND = 3;
	public static final int TYPE_EQUALS_RECOMMEND = 4;
	public static final int TYPE_LESS_RECOMMEND = 5;
	public static final int TYPE_GREATER_RECOMMEND = 6;
	public static final int TYPE_BETWEEN_RECOMMEND = 7;
	public static final int TYPE_MIN_RECOMMEND = 8;
	public static final int TYPE_MAX_RECOMMEND = 9;
	public static final int TYPE_AMBIGUITY = 20;
	
	private static final long serialVersionUID = 1L;

	private String moduleId;

	private List<SuggestedQA> suggestedQAs;
	
	private int type = TYPE_SIMILAR;
	
	private Map<String,Object> attachment;

	public Map<String, Object> getAttachment() {
		return attachment;
	}

	public void setAttachment(Map<String, Object> attachment) {
		this.attachment = attachment;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public List<SuggestedQA> getSuggestedQAs() {
		return suggestedQAs;
	}

	public void setSuggestedQAs(List<SuggestedQA> suggestedQAs) {
		this.suggestedQAs = suggestedQAs;
	}
}
