package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

/**
 * 服务导航回答的<code>Response</code>，包含一个导航命令。为<code>StandardResponse</code>的简化。
 * 
 * @see <code>StandardResponse</code>
 * @author E
 *
 */
public class CommandServiceResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;
	
	private String command;
	private String nodeId;
	private String moduleId;
//	private byte[] attachment;
//	private String[] targetQuestions;
	
	/**
	 * 获取答案对应的指令command
	 * @return 答案对应的指令command
	 */
	public String getCommand() {
		return command;
	}
	/**
	 * 设置答案对应的指令command
	 * @param 答案对应的指令command
	 */
	public void setCommand(String command) {
		this.command = command;
	}
	
	/**
	 * 获取答案对应的节点ID
	 * @return 答案对应的节点ID
	 */
	public String getNodeId() {
		return nodeId;
	}
	/**
	 * 设置答案对应的节点ID
	 * @param nodeId 答案对应的节点ID
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	/**
	 * 获取答案对应的处理模块ID
	 * @return 答案对应的处理模块ID
	 */
	public String getModuleId() {
		return moduleId;
	}
	/**
	 * 设置答案对应的处理模块ID
	 * @param moduleId 答案对应的处理模块ID
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	/**
	 * 获取得分较高的目标标准问题列表</br>
	 * 若此列表长度大于1，调用方可组织问题列表让用户选择，以此提高对问题定位的准确度。
	 * 问题列表是按与用户问题相似度排序的，调用方组织的用户选择列表顺序务须严格与此列表一致。
	 * 另外，类中对应的各字段属性与列表中第一个问题对应，调用方也可直接把答案返回。</br>
	 * </br>
	 * E，补充：(尚未使用，保留扩展)</br>
	 * 只返回如“最近[股票名称]涨了没有”这样的简单模板问题。</br>
	 * 且已经替换完毕，如“最近上海赢思涨了没有”</br>
	 * 故一般返回较短长度的类表（对比<code>StandardResponse</code>）
	 * 
	 * 
	 * @return 得分较高的目标标准问题列表。
	 */
	/*public String[] getTargetQuestions() {
		return targetQuestions;
	}

	/**
	 * 设置得分较高的目标标准问题列表
	 * @param targetQuestions 得分较高的目标标准问题列表。
	 */
	/*public void setTargetQuestions(String[] targetQuestions) {
		this.targetQuestions = targetQuestions;
	}
	
	
	/**
	 * 获取对应节点附属对象
	 * @return 对应节点附属对象
	 */
	/*public byte[] getAttachment() {
		return attachment;
	}
	/**
	 * 设置对应节点附属对象
	 * @param attachment 对应节点附属对象
	 */
	/*public void setAttachment(byte[] attachment) {
		this.attachment = attachment;
	}*/

}
