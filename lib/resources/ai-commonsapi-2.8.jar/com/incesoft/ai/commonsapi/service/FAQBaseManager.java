package com.incesoft.ai.commonsapi.service;

import com.incesoft.ai.commonsapi.service.exception.FAQBaseException;
import com.incesoft.ai.commonsapi.service.model.Answer;
import com.incesoft.ai.commonsapi.service.model.BatchAddRequest;
import com.incesoft.ai.commonsapi.service.model.BatchOperationResult;
import com.incesoft.ai.commonsapi.service.model.BatchRemoveRequest;
import com.incesoft.ai.commonsapi.service.model.FAQNode;
import com.incesoft.ai.commonsapi.service.model.Question;

/**
 * FAQ库管理接口
 * 库的节点类型为{@link com.incesoft.ai.commonsapi.service.model.FAQNode}.
 * 库的结构为列表结构.
 * 
 * @author Boyce Lee
 */
public interface FAQBaseManager {
	
	/**
	 * 添加（或更新）节点到FAQ库
	 * 相当于调用addNodes(moduleId,nodes,false)
	 * @param moduleId 模块ID
	 * @param nodes 被操作的节点数组
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addNodes(String moduleId, FAQNode[] nodes) throws FAQBaseException;
	/**
	 * 添加（或更新）节点到FAQ库
	 * @param moduleId 模块ID
	 * @param nodes 被操作的节点数组
	 * @param rebuild 是否重建索引
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addNodes(String moduleId, FAQNode[] nodes, boolean rebuild) throws FAQBaseException;;
	/**
	 * 从FAQ库删除节点
	 * @param moduleId 模块ID
	 * @param nodeIds 被操作的节点ID数组
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeNodes(String moduleId, String[] nodeIds) throws FAQBaseException;
	
	/**
	 * 从FAQ库添加节点类别树
	 * @param moduleId 模块ID
	 * @param categoryPath 节点所在树路劲（以"/"分隔）
	 * @param nodeIds 被操作的类别ID数组
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addCategory(String moduleId, String categoryPath,String[] nodeIds) throws FAQBaseException;

	/**
	 * 从FAQ库删除类别下所有节点
	 * @param moduleId 模块ID
	 * @param categoryPath 节点所在树路劲（以"/"分隔）
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeCategory(String moduleId, String categoryPath, boolean deleteNodes) throws FAQBaseException;

	/**
	 * 对FAQ库中指定的节点添加（或更新）相似问题。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param questions 被操作的问题数组
	 * @return 操作失败的问题ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addQuestions(String moduleId,String nodeId, Question[] questions) throws FAQBaseException;;
	/**
	 * 从FAQ库中指定的节点删除相似问题。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param questionIds 被操作的问题ID数组
	 * @return 操作失败的问题ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeQuestions(String moduleId,String nodeId, String[] questionIds) throws FAQBaseException;;

	/**
	 * 对FAQ库中指定的节点添加（或更新）相似答案。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param answers 被操作的答案数组
	 * @return 操作失败的答案ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addAnswers(String moduleId,String nodeId, Answer[] answers) throws FAQBaseException;;
	/**
	 * 从FAQ库中指定的节点删除相似答案。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param answerIds 被操作的答案ID数组
	 * @return 操作失败的答案ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeAnswers(String moduleId,String nodeId, String[] answerIds) throws FAQBaseException;
	
	/**
	 * 批量添加节点、问题、答案。
	 * @param moduleId 模块ID
	 * @param request 批量添加请求
	 * @return 批量添加结果，返回null表示成功。
	 * @throws FAQBaseException
	 */
	public BatchOperationResult batchAdd(String moduleId, BatchAddRequest<FAQNode> request) throws FAQBaseException;
	
	/**
	 * 批量删除节点、问题、答案。
	 * @param moduleId 模块ID
	 * @param request 批量删除请求
	 * @return 批量删除结果，返回null表示成功。
	 * @throws FAQBaseException
	 */
	public BatchOperationResult batchRemove(String moduleId, BatchRemoveRequest request) throws FAQBaseException;
	
	/**
	 * 提交对FAQ库的更改
	 * @param moduleId 模块ID
	 * @throws FAQBaseException
	 */
	public BatchOperationResult commit(String moduleId) throws FAQBaseException;
	
	/**
	 * 获取更改FAQ库时的各种状态
	 * @param moduleId 模块ID
	 * @return
	 * @throws FAQBaseException
	 */
	public BatchOperationResult getCommitStatus(String moduleId) throws FAQBaseException;
	
	/**
	 * 提交对FAQ库的更改
	 * @param moduleId 模块ID
	 * @throws FAQBaseException
	 */
	public void commitStep(String moduleId) throws FAQBaseException;
	
	/**
	 * 注解：getCommitStatus和commitStep其实是commit函数分两步走的两个步骤；
	 */
}
