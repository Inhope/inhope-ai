package com.incesoft.ai.commonsapi.service.model.response;

import java.util.List;

import com.incesoft.ai.commonsapi.service.model.AskResponse;
import com.incesoft.ai.commonsapi.service.model.SuggestedWord;

public class WrongSpellResponse extends AskResponse{
	
	private static final long serialVersionUID = 1L;
	
	public static final short TYPE_COMMAND = 1;
	public static final short TYPE_WORD = 2;
	
	private short type = TYPE_COMMAND;

	private List<SuggestedWord> suggestions;
	private List<String> targetQuestions;
	
	public WrongSpellResponse(){}
	
	public WrongSpellResponse(List<SuggestedWord> suggestions) {
		setSuggestions(suggestions);
	}
	public WrongSpellResponse(List<SuggestedWord> suggestions, List<String> targetQuestions) {
		setSuggestions(suggestions);
		setTargetQuestions(targetQuestions);
	}
	public WrongSpellResponse(List<SuggestedWord> suggestions, List<String> targetQuestions, short type) {
		setSuggestions(suggestions);
		setTargetQuestions(targetQuestions);
		setType(type);
	}
	
	public List<SuggestedWord> getSuggestions() {
		return suggestions;
	}

	public void setSuggestions(List<SuggestedWord> suggestions) {
		this.suggestions = suggestions;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public List<String> getTargetQuestions() {
		return targetQuestions;
	}

	public void setTargetQuestions(List<String> targetQuestions) {
		this.targetQuestions = targetQuestions;
	}

}
