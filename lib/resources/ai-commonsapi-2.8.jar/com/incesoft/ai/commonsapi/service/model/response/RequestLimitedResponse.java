package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskRequest;
import com.incesoft.ai.commonsapi.service.model.AskResponse;

public class RequestLimitedResponse extends AskResponse {

	private static final long serialVersionUID = 1L;
	
	private int requestsWindow;
	private int requestsLimit;
	private AskRequest currentRequest;
	
	public RequestLimitedResponse(int requetsLimit, int reqeustsWindow, AskRequest currentRequest) {
		this.requestsLimit = requetsLimit;
		this.requestsWindow = reqeustsWindow;
		this.currentRequest = currentRequest;
	}
	
	public int getRequestsWindow() {
		return requestsWindow;
	}
	public void setRequestsWindow(int requestsWindow) {
		this.requestsWindow = requestsWindow;
	}
	public int getRequestsLimit() {
		return requestsLimit;
	}
	public void setRequestsLimit(int requestsLimit) {
		this.requestsLimit = requestsLimit;
	}
	public AskRequest getCurrentRequest() {
		return currentRequest;
	}
	public void setCurrentRequest(AskRequest currentRequest) {
		this.currentRequest = currentRequest;
	}
}
