package com.incesoft.ai.commonsapi.service.model;

public class ExternalModuleConfiguration extends ModuleConfiguration {
	
	private static final long serialVersionUID = 1L;
	
	private String nodeId;
	private boolean moduleAwared;
	

	public ExternalModuleConfiguration(){
		super();
		nodeId = null;
		moduleAwared = true;
	}
	
	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public boolean isModuleAwared() {
		return moduleAwared;
	}

	public void setModuleAwared(boolean moduleAwared) {
		this.moduleAwared = moduleAwared;
	}
	
	public String toString()
	{
	    final String TAB = ",";
	    
	    String retValue = "";
	    
	    retValue = "ExternalModuleConfiguration ("
	        + super.toString() + TAB
	        + "nodeId = " + this.nodeId + TAB
	        + "moduleAwared = " + this.moduleAwared
	        + " )";
	
	    return retValue;
	}

	
}
