package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;


public class BatchOperationResult implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String[] failedNodeIds;
	private String[] failedQuestionIds;
	private String[] failedAnswerIds;
	
	public String[] getFailedNodeIds() {
		return failedNodeIds;
	}
	public void setFailedNodeIds(String[] failedNodeIds) {
		this.failedNodeIds = failedNodeIds;
	}
	public String[] getFailedQuestionIds() {
		return failedQuestionIds;
	}
	public void setFailedQuestionIds(String[] failedQuestionIds) {
		this.failedQuestionIds = failedQuestionIds;
	}
	public String[] getFailedAnswerIds() {
		return failedAnswerIds;
	}
	public void setFailedAnswerIds(String[] failedAnswerIds) {
		this.failedAnswerIds = failedAnswerIds;
	}


}
