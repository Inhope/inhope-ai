package com.incesoft.ai.commonsapi.service;

import java.util.List;



import com.incesoft.ai.commonsapi.service.exception.LatinSegRuleException;
import com.incesoft.ai.commonsapi.service.exception.PreSuffixException;
import com.incesoft.ai.commonsapi.service.exception.SemanticException;
import com.incesoft.ai.commonsapi.service.exception.StopwordException;
import com.incesoft.ai.commonsapi.service.model.CalcSimilarityResult;
import com.incesoft.ai.commonsapi.service.model.SimilarQuestionPair;
import com.incesoft.ai.commonsapi.service.model.SuggestSemanticResult;
import com.incesoft.ai.commonsapi.service.model.SuggestedSemantic;

public interface SemanticManager {
	
	public List<SuggestedSemantic> suggestSemantic(String question, int top, String moduleId, String[] categories) throws SemanticException;
	public List<SuggestedSemantic> suggestSemantic(String question, int top, String moduleId, String[] categories, int mode) throws SemanticException;
	public SuggestSemanticResult suggestSemantic(String question, int top, String moduleId, String[] categories, int mode, String tracekey) throws SemanticException;
	public List<String> suggestSemantic(List<String> questions, boolean coocurrenceEnabled) throws SemanticException;
	
	public CalcSimilarityResult calcSimilarity(String question1, String question2, String moduleId, String[] categories) throws SemanticException;
	
	public List<SimilarQuestionPair> analyzeSimilarity(List<String> questions, float threshold, String moduleId, String[] categories) throws SemanticException;
	
	public void addStopword(int stopwordLevel, String[] words);
	public void removeStopword(int stopwordLevel, String[] words);
	public void removeStopword(String[] words);
	public void clearStopword();
	public void commitStopword() throws StopwordException;
	
	public void addPrefix(String[] prefix);
	public void removePrefix(String[] prefix);
	public void addSuffix(String[] suffix);
	public void removeSuffix(String[] suffix);
	public void clearPreSuffix();
	public void commitPreSuffix() throws PreSuffixException;
	
	public void addLatinSegRule(String[] rules);
	public void removeLatinSegRule(String[] rules);
	public void clearLatinSegRule();
	public void commitLatinSegRule() throws LatinSegRuleException;

}
