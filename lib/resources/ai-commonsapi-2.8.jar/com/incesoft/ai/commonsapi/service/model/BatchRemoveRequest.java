package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class BatchRemoveRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private LinkedList<String> nodes = new LinkedList<String>();
	private Map<String,List<String>> questions = new HashMap<String,List<String>>();
	private Map<String,List<String>> answers = new HashMap<String,List<String>>();
	
	public void removeNodes(String[] nodeIds) {
		for(String node : nodes) this.nodes.add(node);
	}

	public void removeQuestions(String nodeId, String[] questionIds) {
		List<String> questionList = getRequiredQuestionList(nodeId);
		for(String question:questionIds)questionList.add(question);
	}
	
	public void removeAnswers(String nodeId, String[] answerIds) {
		List<String> answerList = getRequiredAnswerList(nodeId);
		for(String answer: answerIds)answerList.add(answer);
	}

	protected List<String> getRequiredAnswerList(String nodeId) {
		List<String> answerList = answers.get(nodeId);
		if(answerList == null) {
			answerList = new LinkedList<String>();
			answers.put(nodeId, answerList);
		}
		return answerList;
	}
	protected List<String> getRequiredQuestionList(String nodeId) {
		List<String> questionList = questions.get(nodeId);
		if(questionList == null) {
			questionList = new LinkedList<String>();
			questions.put(nodeId, questionList);
		}
		return questionList;
	}
	
	public LinkedList<String> getNodes() {
		return nodes;
	}

	public void setNodes(LinkedList<String> nodes) {
		this.nodes = nodes;
	}

	public Map<String, List<String>> getQuestions() {
		return questions;
	}

	public void setQuestions(Map<String, List<String>> questions) {
		this.questions = questions;
	}

	public Map<String, List<String>> getAnswers() {
		return answers;
	}

	public void setAnswers(Map<String, List<String>> answers) {
		this.answers = answers;
	}

}
