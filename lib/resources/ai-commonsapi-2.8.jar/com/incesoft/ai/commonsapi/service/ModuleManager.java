package com.incesoft.ai.commonsapi.service;

import com.incesoft.ai.commonsapi.service.exception.ModuleException;
import com.incesoft.ai.commonsapi.service.model.ModuleConfiguration;

public interface ModuleManager {
	
	public ModuleConfiguration[] getModuleConfigurations() throws ModuleException;
	
	public ModuleConfiguration getModuleConfiguration(String moduleId) throws ModuleException;

	public void addModule(ModuleConfiguration config) throws ModuleException;
	
	public void removeModule(String moduleId) throws ModuleException;
	
	public void clearModule(String moduleId) throws ModuleException;
	
	public void updateModule(ModuleConfiguration config) throws ModuleException;
	
}
