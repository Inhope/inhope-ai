package com.incesoft.ai.commonsapi.service;

import java.util.List;
import java.util.Map;

import com.incesoft.ai.commonsapi.algorithm.exception.SegmentException;
import com.incesoft.ai.commonsapi.algorithm.model.Pinyin;
import com.incesoft.ai.commonsapi.service.exception.PinyinException;
import com.incesoft.ai.commonsapi.service.exception.WordClassException;

/**
 * 词类库管理
 * @author Boyce Lee
 */
public interface WordClassManager {
	
	/**
	 * 
	 * @param path
	 * @throws WordClassException
	 */
	public void addWordClass(String path) throws WordClassException;
	
	/**
	 * 
	 * @param path
	 * @throws WordClassException
	 */
	public void removeWordClass(String path) throws WordClassException;
	
	/**
	 * 
	 * @param path
	 * @param oldname
	 * @param newname
	 * @throws WordClassException
	 */
	public void renameWordClass(String path,String oldname,String newname) throws WordClassException;
	
	/**
	 * 添加词到词类库
	 * @param path 词路径，路径以斜杠（/）分割
	 * @param words 所添加的词列表
	 * @throws WordClassException
	 */
	public void addWords(String path, String[] words) throws WordClassException;
	
	/**
	 * 从词类库删除词
	 * @param path 词路径，路径以斜杠（/）分割
	 * @param words 所要删除的词列表
	 * @throws WordClassException
	 */
	public void removeWords(String path, String[] words) throws WordClassException;
	
	/**
	 * 往基础词库里添加词
	 * @param words
	 * @return
	 * @throws SegmentException
	 */
	public void addWords(String[] words) throws WordClassException;
	
	/**
	 * 从基础词库删除词
	 * @param words
	 * @throws WordClassException
	 */
	public void removeWords(String[] words) throws WordClassException;
	
	/**
	 * 从基础词库中搜索某个词
	 * @param name
	 * @return
	 * @throws WordClassException
	 */
	public String[] searchWord(String name) throws WordClassException;
	
	public String[] searchWord(String name, boolean basewordOnly) throws WordClassException;
	
	/**
	 * 恢复基础词库
	 * @return
	 * @throws WordClassException
	 */
	public boolean restoreWords() throws WordClassException;
	
	/**
	 * 清空基础词库
	 * @return
	 * @throws WordClassException
	 */
	public boolean clearWords() throws WordClassException;
	
	/**
	 * 向拼音纠错模块中添加词以及对应的拼音
	 * 
	 * @param word
	 * @param pinyin
	 * @throws PinyinException
	 */
	public void addWordPinyin(Map<String, List<Pinyin>> words) throws PinyinException;
	
	/**
	 * 移除拼音纠错模块中的纠错词所属的拼音
	 * 
	 * @param word
	 * @throws PinyinException
	 */
	public void removeWordPinyin(List<String> words) throws PinyinException;
	
	/**
	 * 取得一个字或者几个字的拼音，多字时以" "（空格）分隔。
	 * 
	 * @param words
	 * @return
	 * @throws PinyinException
	 */
	public String getPinyin(String words) throws PinyinException;
	
	/**
	 * 清除所有单词的拼音，只保留字的拼音。
	 * 
	 * @throws PinyinException
	 */
	public void clearAllWordPinyin() throws PinyinException;
	
	public void commit() throws WordClassException;
	
}
