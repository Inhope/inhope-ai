package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;

public class ModuleConfiguration implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String moduleId;
	private String name;
	private int priority;
	private boolean enable;
	
	private String nodeId;
	private String nodeAddrs;
	private boolean moduleAwared;
	
	private boolean contextAwared;
	private int ignoreCategory;
	private float scoreThreshold;
	private int maxRetryTimes;
	private String[] excludedWordNodes;
	private int capability;
	private byte wordclassSearchDepth;
	
	private float searchThreshold;
	private float minScoreThreshold;
	private float minSimilarityDiff;
	private float maxSimilarityDiff;
	private float contextObjectBoost;
	private float contextClassAttrBoost;
	private float contextAskNodeBoost;
	
	public ModuleConfiguration(){
		moduleId = null;
		name = null;
		priority = 3;
		enable = true;
		
		nodeId = null;
		moduleAwared = true;
		
		contextAwared = false;
		
		ignoreCategory = 0;
		scoreThreshold = (float)Math.cos(Math.PI/6);
		maxRetryTimes = 1;
		excludedWordNodes = null;
		capability = 0;
		
		searchThreshold = (float)Math.cos(Math.PI/4);
		contextObjectBoost = -0.001f;
		contextClassAttrBoost = 0.001f;
		contextAskNodeBoost = 0.001f;
		
		maxSimilarityDiff = 0f;
		minSimilarityDiff = 0.0005f;
		
		wordclassSearchDepth = Byte.parseByte(System.getProperty("wordclassCategorySearchDepth","0"));
	}
	
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public int getIgnoreCategory() {
		return ignoreCategory;
	}
	public void setIgnoreCategory(int ignoreCategory) {
		this.ignoreCategory = ignoreCategory;
	}
	public float getScoreThreshold() {
		return scoreThreshold;
	}
	public void setScoreThreshold(float scoreThreshold) {
		this.scoreThreshold = scoreThreshold;
	}
	public int getMaxRetryTimes() {
		return maxRetryTimes;
	}
	public void setMaxRetryTimes(int maxRetryTimes) {
		this.maxRetryTimes = maxRetryTimes;
	}
	public String[] getExcludedWordNodes() {
		return excludedWordNodes;
	}
	public void setExcludedWordNodes(String[] excludedWordNodes) {
		this.excludedWordNodes = excludedWordNodes;
	}
	public int getCapability() {
		return capability;
	}
	public void setCapability(int capability) {
		this.capability = capability;
	}
	public float getMinSimilarityDiff() {
		return minSimilarityDiff;
	}

	public void setMinSimilarityDiff(float minSimilarityDiff) {
		this.minSimilarityDiff = minSimilarityDiff;
	}

	public float getMaxSimilarityDiff() {
		return maxSimilarityDiff;
	}

	public void setMaxSimilarityDiff(float maxSimilarityDiff) {
		this.maxSimilarityDiff = maxSimilarityDiff;
	}
	
	public String toString()
	{
	    final String TAB = ",";
	    
	    String retValue = "";
	    
	    retValue = "ModuleConfiguration ("
	        + "moduleId = " + this.moduleId + TAB
	        + "name = " + this.name + TAB
	        + "priority = " + this.priority + TAB
	        + "enable = " + this.enable + TAB
	        + "contextAwared = " + this.contextAwared + TAB
	        + "ignoreCategory = " + this.ignoreCategory + TAB
	        + "scoreThreshold = " + this.scoreThreshold + TAB
	        + "searchThreshold = " + this.searchThreshold + TAB
	        + "minSimilarityDiff = " + this.minSimilarityDiff + TAB
	        + "maxSimilarityDiff = " + this.maxSimilarityDiff + TAB
	        + "maxRetryTimes = " + this.maxRetryTimes + TAB
	        + "excludedWordNodes = " + this.excludedWordNodes + TAB
	        + "capability = " + this.capability + TAB
	        + "contextObjectBoost = " + this.contextObjectBoost + TAB
	        + "contextClassAttrBoost = " + this.contextClassAttrBoost + TAB
	        + "contextAskNodeBoost = " + this.contextAskNodeBoost + TAB
	        + "wordclassSearchDepth = " + this.wordclassSearchDepth + TAB
	        + "nodeId = " + this.nodeId + TAB
	        + "nodeAddrs = " + this.nodeAddrs + TAB
	        + "moduleAwared = " + this.moduleAwared
	        + " )";
	
	    return retValue;
	}
	public boolean isContextAwared() {
		return contextAwared;
	}
	public void setContextAwared(boolean contextAwared) {
		this.contextAwared = contextAwared;
	}
	
	public float getSearchThreshold() {
		return searchThreshold;
	}
	public void setSearchThreshold(float searchThreshold) {
		this.searchThreshold = searchThreshold;
	}
	public float getMinScoreThreshold() {
		return minScoreThreshold;
	}

	public void setMinScoreThreshold(float minScoreThreshold) {
		this.minScoreThreshold = minScoreThreshold;
	}
	public float getContextObjectBoost() {
		return contextObjectBoost;
	}
	public void setContextObjectBoost(float contextObjectBoost) {
		this.contextObjectBoost = contextObjectBoost;
	}
	public float getContextClassAttrBoost() {
		return contextClassAttrBoost;
	}
	public void setContextClassAttrBoost(float contextClassAttrBoost) {
		this.contextClassAttrBoost = contextClassAttrBoost;
	}
	public float getContextAskNodeBoost() {
		return contextAskNodeBoost;
	}
	public void setContextAskNodeBoost(float contextAskNodeBoost) {
		this.contextAskNodeBoost = contextAskNodeBoost;
	}
	
	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	public String getNodeAddrs() {
		return nodeAddrs;
	}

	public void setNodeAddrs(String nodeAddrs) {
		this.nodeAddrs = nodeAddrs;
	}

	public byte getWordclassSearchDepth() {
		return wordclassSearchDepth;
	}

	public void setWordclassSearchDepth(byte wordclassSearchDepth) {
		this.wordclassSearchDepth = wordclassSearchDepth;
	}
	
	public boolean isModuleAwared() {
		return moduleAwared;
	}

	public void setModuleAwared(boolean moduleAwared) {
		this.moduleAwared = moduleAwared;
	}
}
