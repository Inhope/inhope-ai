package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;

public class SuggestedSemantic implements Serializable {

	float score;
	String semanticId;
	String semanticName;
	String matchedRule;
	Map<String,String> semanticBlocks;
	
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public String getSemanticId() {
		return semanticId;
	}
	public void setSemanticId(String semanticId) {
		this.semanticId = semanticId;
	}
	public String getSemanticName() {
		return semanticName;
	}
	public void setSemanticName(String semanticName) {
		this.semanticName = semanticName;
	}
	public String getMatchedRule() {
		return matchedRule;
	}
	public void setMatchedRule(String matchedRule) {
		this.matchedRule = matchedRule;
	}
	public Map<String, String> getSemanticBlocks() {
		return semanticBlocks;
	}
	public void setSemanticBlocks(Map<String, String> semanticBlocks) {
		this.semanticBlocks = semanticBlocks;
	}
}
