package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class BatchAddRequest<T extends Node> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private LinkedList<T> nodes = new LinkedList<T>();
	private Map<String,List<Question>> questions = new HashMap<String,List<Question>>();
	private Map<String,List<Answer>> answers = new HashMap<String,List<Answer>>();
	
	public void addNodes(T[] nodes) {
		for(T node : nodes) this.nodes.add(node);
	}

	public void addQuestions(String nodeId, Question[] questions) {
		List<Question> questionList = getRequiredQuestionList(nodeId);
		for(Question question:questions)questionList.add(question);
	}
	
	public void addAnswers(String nodeId, Answer[] answers) {
		List<Answer> answerList = getRequiredAnswerList(nodeId);
		for(Answer answer: answers)answerList.add(answer);
	}

	protected List<Answer> getRequiredAnswerList(String nodeId) {
		List<Answer> answerList = answers.get(nodeId);
		if(answerList == null) {
			answerList = new LinkedList<Answer>();
			answers.put(nodeId, answerList);
		}
		return answerList;
	}
	protected List<Question> getRequiredQuestionList(String nodeId) {
		List<Question> questionList = questions.get(nodeId);
		if(questionList == null) {
			questionList = new LinkedList<Question>();
			questions.put(nodeId, questionList);
		}
		return questionList;
	}
	
	public LinkedList<T> getNodes() {
		return nodes;
	}

	public void setNodes(LinkedList<T> nodes) {
		this.nodes = nodes;
	}

	public Map<String, List<Question>> getQuestions() {
		return questions;
	}

	public void setQuestions(Map<String, List<Question>> questions) {
		this.questions = questions;
	}

	public Map<String, List<Answer>> getAnswers() {
		return answers;
	}

	public void setAnswers(Map<String, List<Answer>> answers) {
		this.answers = answers;
	}

}
