package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

/**
 * 针对用户对问题列表选择的错误输入响应
 * @author Boyce Lee
 */
public class MisinputResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;

	public final static short TYPE_LESS = -1;
	public final static short TYPE_GREATER = 1;
	private String remind;
	private int listLength;
	
	private short type = -1;

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public String getRemind() {
		return remind;
	}

	public void setRemind(String remind) {
		this.remind = remind;
	}

	public int getListLength() {
		return listLength;
	}

	public void setListLength(int listLength) {
		this.listLength = listLength;
	}
	
	
}
