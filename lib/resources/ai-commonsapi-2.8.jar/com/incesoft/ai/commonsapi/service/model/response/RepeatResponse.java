/**
 * 
 */
package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

/**
 * @author norman
 *
 */
public class RepeatResponse extends AskResponse{

	private static final long serialVersionUID = 1L;
}
