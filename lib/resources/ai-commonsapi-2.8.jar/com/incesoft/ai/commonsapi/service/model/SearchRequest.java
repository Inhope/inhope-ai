package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;
/**
 * 搜索输入类型
 * 其中问题类别与Question类型中的类别相关联
 * @author norman
 *
 */
public class SearchRequest implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public static final short MODE_SIMILARITY = 1;
	public static final short MODE_KEYWORD = 2;
	
	private String question;
	private String hotWords;
	private String[] categories;
	private String moduleId;
	private Map<String,String> filter;//答案维度的过滤器
	private short maxReturn;
	private short searchMode = MODE_SIMILARITY;
	private boolean wordInfoRequired;

	/**
	 * 获取问题内容
	 * @return 问题内容
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * 设置问题内容
	 * @param question 问题内容
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * 获取热榜词汇
	 * @return 热榜词汇
	 */
	public String getHotWords() {
		return hotWords;
	}
	/**
	 * 设置热榜词汇
	 * @param hotWords 热榜词汇
	 */
	public void setHotWords(String hotWords) {
		this.hotWords = hotWords;
	}
	/**
	 * 获取问题类别
	 * @return 问题类别
	 */
	public String[] getCategories() {
		return categories;
	}
	/**
	 * 设置问题类别
	 * @param categories 问题类别
	 */
	public void setCategories(String[] categories) {
		this.categories = categories;
	}
	/**
	 *  获取最大返回个数
	 * @return maxReturn
	 */
	public short getMaxReturn() {
		return maxReturn;
	}
	/**
	 * 设置最大返回个数
	 * @param maxReturn  最大返回个数
	 */
	public void setMaxReturn(short maxReturn) {
		this.maxReturn = maxReturn;
	}
	/**
	 * 获取搜索模式
	 * @return searchMode
	 */
	public short getSearchMode() {
		return searchMode;
	}
	/**
	 * 设置搜索模式
	 * @param searchMode 搜索模式
	 */
	public void setSearchMode(short searchMode) {
		this.searchMode = searchMode;
	}
	public Map<String, String> getFilter() {
		return filter;
	}
	public void setFilter(Map<String, String> filter) {
		this.filter = filter;
	}
	public boolean isWordInfoRequired() {
		return wordInfoRequired;
	}
	public void setWordInfoRequired(boolean wordInfoRequired) {
		this.wordInfoRequired = wordInfoRequired;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	
}
