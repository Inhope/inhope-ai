package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;

public class SuggestedQA implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String nodeId;
	private String answer;
	private String question;
	private Map<String,Object> attachment;
	private String[] tags;
	
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public Map<String, Object> getAttachment() {
		return attachment;
	}
	public void setAttachment(Map<String, Object> attachment) {
		this.attachment = attachment;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
}
