package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;

/**
 * 回答给用户的答案
 * 答案的概念准确的定义应该为引擎输出的内容，使用时务虚注意！
 * 比如在情景中用户对机器人反问的回答并不是此类所定义的答案概念。
 * @author Boyce Lee
 */
public class Answer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String content;
	private Map<String,Object> attachment;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Map<String, Object> getAttachment() {
		return attachment;
	}
	public void setAttachment(Map<String, Object> attachment) {
		this.attachment = attachment;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Answer other = (Answer) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Answer clone() {
		Answer answer = new Answer();
		answer.setId(id);
		answer.setContent(content);
		answer.setAttachment(attachment);
		return answer;
	}
	
}
