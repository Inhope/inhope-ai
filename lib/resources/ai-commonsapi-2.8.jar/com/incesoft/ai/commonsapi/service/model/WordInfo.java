package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;

/**
 * 词描述信息
 * 
 * @author Boyce Lee
 */
public class WordInfo implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String content;
	private Map<String,String> attributes;
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Map<String, String> getAttributes() {
		return attributes;
	}
	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}
}
