package com.incesoft.ai.commonsapi.service;

import com.incesoft.ai.commonsapi.service.exception.LicenceException;
import com.incesoft.ai.commonsapi.service.model.Licence;

public interface LicenceService {

	public Licence validate() throws LicenceException;
	
	public int authenticate(String credential) throws LicenceException;
	
	public Licence decode(byte[] data) throws LicenceException;
	
}
