package com.incesoft.ai.commonsapi.service.model.response;

import java.util.List;

import com.incesoft.ai.commonsapi.service.model.AskResponse;
import com.incesoft.ai.commonsapi.service.model.ExternalModule;

public class FlowResponse extends AskResponse {

	private static final long serialVersionUID = 1L;
	
	private List<ExternalModule> externalModules;
	private List<String> unprocessedModuleIds;
	
	public List<ExternalModule> getExternalModules() {
		return externalModules;
	}
	public void setExternalModules(List<ExternalModule> externalModules) {
		this.externalModules = externalModules;
	}
	public List<String> getUnprocessedModuleIds() {
		return unprocessedModuleIds;
	}
	public void setUnprocessedModuleIds(List<String> unprocessedModuleIds) {
		this.unprocessedModuleIds = unprocessedModuleIds;
	}
	
	
	
}
