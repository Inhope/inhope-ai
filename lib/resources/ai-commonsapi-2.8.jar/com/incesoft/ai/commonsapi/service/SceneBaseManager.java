package com.incesoft.ai.commonsapi.service;


import com.incesoft.ai.commonsapi.service.exception.SceneBaseException;
import com.incesoft.ai.commonsapi.service.model.Answer;
import com.incesoft.ai.commonsapi.service.model.BatchAddRequest;
import com.incesoft.ai.commonsapi.service.model.BatchOperationResult;
import com.incesoft.ai.commonsapi.service.model.BatchRemoveRequest;
import com.incesoft.ai.commonsapi.service.model.Question;
import com.incesoft.ai.commonsapi.service.model.SceneNode;

/**
 * 情景库管理接口
 * 库的节点类型为{@link com.incesoft.ai.commonsapi.service.model.SceneNode}.
 * 库的结构为树状结构.
 * 
 * @author Boyce Lee
 */
public interface SceneBaseManager {

	/**
	 * 添加（或更新）节点到情景库
	 * 相当于调用addNodes(moduleId,nodes,false)
	 * @param moduleId 模块ID
	 * @param nodes 被操作的节点数组
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addNodes(String moduleId, SceneNode[] nodes) throws SceneBaseException;
	/**
	 * 添加（或更新）节点到情景库
	 * @param moduleId 模块ID
	 * @param nodes 被操作的节点数组
	 * @param rebuild 是否重建索引
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addNodes(String moduleId, SceneNode[] nodes, boolean rebuild) throws SceneBaseException;;
	/**
	 * 从情景库删除节点
	 * @param moduleId 模块ID
	 * @param nodeIds 被操作的节点ID数组
	 * @return 操作失败的节点ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeNodes(String moduleId,String[] nodeIds) throws SceneBaseException;;

	/**
	 * 对情景库中指定的节点添加（或更新）相似问题。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param questions 被操作的问题数组
	 * @return 操作失败的问题ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addQuestions(String moduleId,String nodeId, Question[] questions) throws SceneBaseException;;
	/**
	 * 从情景库中指定的节点删除相似问题。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param questionIds 被操作的问题ID数组
	 * @return 操作失败的问题ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeQuestions(String moduleId,String nodeId, String[] questionIds) throws SceneBaseException;;
	
	/**
	 * 对情景库中指定的节点添加（或更新）相似答案。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param answers 被操作的答案数组
	 * @return 操作失败的答案ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] addAnswers(String moduleId,String nodeId, Answer[] answers) throws SceneBaseException;;
	/**
	 * 从情景库中指定的节点删除相似答案。
	 * @param moduleId 模块ID
	 * @param nodeId 节点ID
	 * @param answerIds 被操作的答案ID数组
	 * @return 操作失败的答案ID数组，返回null或空数组表示操作全部成功。
	 */
	public String[] removeAnswers(String moduleId,String nodeId, String[] answerIds) throws SceneBaseException;;
	
	/**
	 * 批量添加节点、问题、答案。
	 * @param moduleId 模块ID
	 * @param request 批量添加请求
	 * @return 批量添加结果，返回null表示成功。
	 * @throws FAQBaseException
	 */
	public BatchOperationResult batchAdd(String moduleId, BatchAddRequest<SceneNode> request) throws SceneBaseException;
	
	/**
	 * 批量删除节点、问题、答案。
	 * @param moduleId 模块ID
	 * @param request 批量删除请求
	 * @return 批量删除结果，返回null表示成功。
	 * @throws FAQBaseException
	 */
	public BatchOperationResult batchRemove(String moduleId, BatchRemoveRequest request) throws SceneBaseException;
	
	/**
	 * 提交对情景库的更改
	 * @param moduleId 模块ID
	 * @throws FAQBaseException
	 */
	public BatchOperationResult commit(String moduleId) throws SceneBaseException;
}
