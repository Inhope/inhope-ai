package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;

public class SimilarQuestionPair implements Serializable {
	public String question1;
	public String question2;
	public double similarity;
	
	public SimilarQuestionPair(String question1, String question2, double similarity) {
		this.question1 = question1;
		this.question2 = question2;
		this.similarity = similarity;
	}
	
	public String getQuestion1() {
		return question1;
	}
	public void setQuestion1(String question1) {
		this.question1 = question1;
	}
	public String getQuestion2() {
		return question2;
	}
	public void setQuestion2(String question2) {
		this.question2 = question2;
	}
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
}
