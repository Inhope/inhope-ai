package com.incesoft.ai.commonsapi.service;

import com.incesoft.ai.commonsapi.service.exception.AskException;
import com.incesoft.ai.commonsapi.service.model.AskRequest;
import com.incesoft.ai.commonsapi.service.model.AskResponse;

/**
 * 问答服务接口
 * @author Boyce Lee
 */
public interface AskService {
	
	/**
	 * 问答服务处理方法
	 * @param request 问答请求
	 * @return	问答响应
	 */
	public AskResponse process(AskRequest request) throws AskException;
	
}
