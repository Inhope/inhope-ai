package com.incesoft.ai.commonsapi.service;

import com.incesoft.ai.commonsapi.service.exception.SearchException;
import com.incesoft.ai.commonsapi.service.model.SearchRequest;
import com.incesoft.ai.commonsapi.service.model.response.SuggestedResponse;

/**
 * 搜索服务接口
 * @author norman
 *
 */
public interface SearchService {

	/**
	 * 搜索服务处理方法
	 * @param request 搜索请求
	 * @return  搜索响应
	 * @throws SearchException
	 */
	public SuggestedResponse process(SearchRequest request) throws SearchException;
}
