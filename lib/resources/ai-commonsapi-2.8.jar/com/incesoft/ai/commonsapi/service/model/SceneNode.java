package com.incesoft.ai.commonsapi.service.model;

/**
 * 情景库节点类型
 * @author Boyce Lee
 */
public class SceneNode extends Node {
	
	private static final long serialVersionUID = 1L;
	
	public static final short ANSWER_STANDARD = 100;
	
	public static final short ANSWER_RANDOM = 101;
	
	public static final short ANSWER_CONDITIONAL = 102;
	
	public static final short ANSWER_FIRST = 103;
	
	private String parentNodeId;
	private boolean accessable;
	private boolean leaf;
	private short answerType = ANSWER_STANDARD;
	/**
	 * 获取父节点ID
	 * @return 父节点ID
	 */
	public String getParentNodeId() {
		return parentNodeId;
	}
	/**
	 * 设置父节点ID
	 * @param parentNodeId 父节点ID
	 */
	public void setParentNodeId(String parentNodeId) {
		this.parentNodeId = parentNodeId;
	}
	/**
	 * 判断此节点是否可以作为情景的入口
	 * @return True - 可以作为情景入口， False - 不能作为入口
	 */
	public boolean isAccessable() {
		return accessable;
	}
	/**
	 * 设置此节点是否可以作为情景的入口
	 * @param accessable True - 可以作为情景入口， False - 不能作为入口
	 */
	public void setAccessable(boolean accessable) {
		this.accessable = accessable;
	}
	/**
	 * 判断此节点是否为叶子节点
	 * @return True - 是叶子节点， False - 不是叶子节点
	 */
	public boolean isLeaf() {
		return leaf;
	}
	/**
	 * 设置此节点是否为叶子节点
	 * @param leaf True - 是叶子节点， False - 不是叶子节点
	 */
	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}
	public short getAnswerType() {
		return answerType;
	}
	public void setAnswerType(short answerType) {
		this.answerType = answerType;
	}
	
}
