package com.incesoft.ai.commonsapi.service;

import com.incesoft.ai.commonsapi.service.exception.ResourceException;

public interface ResourcesManager {
	
	/**
	 * 更新指定模块的权重表
	 * @param baseId 模块的ID，如"faq"，"chat"，"scene"等。
	 * @throws ResourceException
	 */
	public void refreshWeightMap(String baseId) throws ResourceException;
	
	/**
	 * 更新所有模块的权重表
	 * @throws ResourceException
	 */
	public void refreshAllWeightMap() throws ResourceException;
	
	/**
	 * 更新指定模块的搜索需要的Term列表
	 * @param baseId 模块的ID，如"faq"，"chat"，"scene"等。
	 * @throws ResourceException
	 */
	public void refreshSearchTermMap(String baseId) throws ResourceException;
	
	/**
	 * 更新所有模块的搜索需要的Term列表
	 * @throws ResourceException
	 */
	public void refreshAllSearchTermMaps() throws ResourceException;
	
}
