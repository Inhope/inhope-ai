package com.incesoft.ai.commonsapi.service.model;

public class InternalModuleConfiguration extends ModuleConfiguration {
	
	private static final long serialVersionUID = 1L;
	
	private boolean contextAwared;
	private int ignoreCategory;
	private float scoreThreshold;
	private int maxRetryTimes;
	private String[] excludedWordNodes;
	private int capability;
	private byte wordclassSearchDepth;
	private float searchThreshold;
	private float contextObjectBoost;
	private float contextClassAttrBoost;
	private float contextAskNodeBoost;
	
	public InternalModuleConfiguration(){

		contextAwared = false;
		
		ignoreCategory = 0;
		scoreThreshold = (float)Math.cos(Math.PI/6);
		maxRetryTimes = 1;
		excludedWordNodes = null;
		capability = 0;
		
		searchThreshold = (float)Math.cos(Math.PI/4);
		contextObjectBoost = -0.001f;
		contextClassAttrBoost = 0.001f;
		contextAskNodeBoost = 0.001f;
	}
	
	public int getIgnoreCategory() {
		return ignoreCategory;
	}
	public void setIgnoreCategory(int ignoreCategory) {
		this.ignoreCategory = ignoreCategory;
	}
	public float getScoreThreshold() {
		return scoreThreshold;
	}
	public void setScoreThreshold(float scoreThreshold) {
		this.scoreThreshold = scoreThreshold;
	}
	public int getMaxRetryTimes() {
		return maxRetryTimes;
	}
	public void setMaxRetryTimes(int maxRetryTimes) {
		this.maxRetryTimes = maxRetryTimes;
	}
	public String[] getExcludedWordNodes() {
		return excludedWordNodes;
	}
	public void setExcludedWordNodes(String[] excludedWordNodes) {
		this.excludedWordNodes = excludedWordNodes;
	}
	public int getCapability() {
		return capability;
	}
	public void setCapability(int capability) {
		this.capability = capability;
	}
	public byte getWordclassSearchDepth() {
		return wordclassSearchDepth;
	}

	public void setWordclassSearchDepth(byte wordclassSearchDepth) {
		this.wordclassSearchDepth = wordclassSearchDepth;
	}
	
	public String toString()
	{
	    final String TAB = ",";
	    
	    String retValue = "";
	    
	    retValue = "ModuleConfiguration ("
	        + super.toString() + TAB
	        + "contextAwared = " + this.contextAwared + TAB
	        + "ignoreCategory = " + this.ignoreCategory + TAB
	        + "scoreThreshold = " + this.scoreThreshold + TAB
	        + "maxRetryTimes = " + this.maxRetryTimes + TAB
	        + "excludedWordNodes = " + this.excludedWordNodes + TAB
	        + "capability = " + this.capability + TAB
	        + "searchThreshold = " + this.searchThreshold + TAB
	        + "contextObjectBoost = " + this.contextObjectBoost + TAB
	        + "contextClassAttrBoost = " + this.contextClassAttrBoost + TAB
	        + "contextAskNodeBoost = " + this.contextAskNodeBoost
	        + "wordclassSearchDepth = " + this.wordclassSearchDepth
	        + " )";
	
	    return retValue;
	}
	public boolean isContextAwared() {
		return contextAwared;
	}
	public void setContextAwared(boolean contextAwared) {
		this.contextAwared = contextAwared;
	}
	
	public float getSearchThreshold() {
		return searchThreshold;
	}
	public void setSearchThreshold(float searchThreshold) {
		this.searchThreshold = searchThreshold;
	}
	public float getContextObjectBoost() {
		return contextObjectBoost;
	}
	public void setContextObjectBoost(float contextObjectBoost) {
		this.contextObjectBoost = contextObjectBoost;
	}
	public float getContextClassAttrBoost() {
		return contextClassAttrBoost;
	}
	public void setContextClassAttrBoost(float contextClassAttrBoost) {
		this.contextClassAttrBoost = contextClassAttrBoost;
	}
	public float getContextAskNodeBoost() {
		return contextAskNodeBoost;
	}
	public void setContextAskNodeBoost(float contextAskNodeBoost) {
		this.contextAskNodeBoost = contextAskNodeBoost;
	}
	
	
}
