
package com.incesoft.ai.commonsapi.service.model.response;

import java.util.Map;

import com.incesoft.ai.commonsapi.service.model.AskResponse;
import com.incesoft.ai.commonsapi.service.model.Node;

/**
 * 问答输出类型
 * 其中{@link #getAttachment() attachment}与知识库中录入的Node类型中的{@link Node#getAttachment() attachment}相关。
 * @see Node
 * @author Boyce Lee
 */
public class StandardResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;
	
	private String answer;
	private String nodeId;
	private String moduleId;
	private Map<String,Object> attachment;
	private String[] tags;
	private String[] targetQuestions;
	private float similarity;
	
	
	/**
	 * 获取答案内容
	 * @return 答案内容
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * 设置答案内容
	 * @param answer 答案内容
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	/**
	 * 获取答案对应的节点ID
	 * @return 答案对应的节点ID
	 */
	public String getNodeId() {
		return nodeId;
	}
	/**
	 * 设置答案对应的节点ID
	 * @param nodeId 答案对应的节点ID
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	
	/**
	 * 获取答案对应的处理模块ID
	 * @return 答案对应的处理模块ID
	 */
	public String getModuleId() {
		return moduleId;
	}
	/**
	 * 设置答案对应的处理模块ID
	 * @param moduleId 答案对应的处理模块ID
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	/**
	 * 获取得分较高的目标标准问题列表
	 * 若此列表长度大于1，调用方可组织问题列表让用户选择，以此提高对问题定位的准确度。
	 * 问题列表是按与用户问题相似度排序的，调用方组织的用户选择列表顺序务须严格与此列表一致。
	 * 另外，类中对应的各字段属性与列表中第一个问题对应，调用方也可直接把答案返回。
	 * @return 得分较高的目标标准问题列表。
	 */
	public String[] getTargetQuestions() {
		return targetQuestions;
	}

	/**
	 * 设置得分较高的目标标准问题列表
	 * @param targetQuestions 得分较高的目标标准问题列表。
	 */
	public void setTargetQuestions(String[] targetQuestions) {
		this.targetQuestions = targetQuestions;
	}
	/**
	 * 获取对应节点附属对象
	 * @return 对应节点附属对象
	 */
	public Map<String, Object> getAttachment() {
		return attachment;
	}
	/**
	 * 设置对应节点附属对象
	 * @param attachment 对应节点附属对象
	 */
	public void setAttachment(Map<String, Object> attachment) {
		this.attachment = attachment;
	}
	/**
	 * 获得对应知识点检索标签
	 * @return 知识点检索标签
	 */
	public String[] getTags() {
		return tags;
	}
	/**
	 * 设置对应知识点检索标签
	 * @param tags
	 */
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	
	public float getSimilarity() {
		return similarity;
	}
	public void setSimilarity(float similarity) {
		this.similarity = similarity;
	}
}
