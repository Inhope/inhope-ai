package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

public class ReverseAskResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;
	
	private String question;
	
	public ReverseAskResponse(){}
	
	public ReverseAskResponse(String question) {
		this.question = question;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

}
