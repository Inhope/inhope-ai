package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Map;

/**
 * 知识库节点基类型
 * 包含标准问题及标准答案，一个节点只对应一个标准问题和一个标准答案
 * 若需要给节点添加相似问题或相似答案，可参考对应知识库的管理接口
 * @author Boyce Lee
 */
public class Node implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	protected String nodeId;
	protected Question question;
	protected Answer answer;
	protected String[] tags;
	protected Map<String,Object> attachment;
	
	/**
	 * 获取节点ID
	 * @return 节点ID
	 */
	public String getNodeId() {
		return nodeId;
	}
	/**
	 * 设置节点ID
	 * @param nodeId 节点ID
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	/**
	 * 获取标准问题
	 * @return 标准问题
	 */
	public Question getQuestion() {
		return question;
	}
	/**
	 * 设置标准问题
	 * @param question 标准问题
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}
	/**
	 * 获取标准答案
	 * @return 标准答案
	 */
	public Answer getAnswer() {
		return answer;
	}
	/**
	 * 设置标准答案
	 * @param answer 标准答案
	 */
	public void setAnswer(Answer answer) {
		this.answer = answer;
	}
	
	/**
	 * 获取检索标签
	 * @return 检索标签
	 */
	public String[] getTags() {
		return tags;
	}
	
	/**
	 * 设置检索标签
	 * @param tags 检索标签
	 */
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	
	/**
	 * 获取节点附属对象
	 * @return 节点附属对象
	 */
	public Map<String, Object> getAttachment() {
		return attachment;
	}
	/**
	 * 设置节点附属对象
	 * @param attachment 节点附属对象
	 */
	public void setAttachment(Map<String, Object> attachment) {
		this.attachment = attachment;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Node other = (Node) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}
	
	
}
