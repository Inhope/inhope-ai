package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;

public class IllegalWordResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;
	
	private String illegalWord = null;
	private String type = null;
	public IllegalWordResponse(){}
	
	public IllegalWordResponse(String illegalWord,String type) {
		this.type = type;
		this.illegalWord = illegalWord;
	}


	public String getIllegalWord() {
		return illegalWord;
	}

	public void setIllegalWord(String illegalWord) {
		this.illegalWord = illegalWord;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
