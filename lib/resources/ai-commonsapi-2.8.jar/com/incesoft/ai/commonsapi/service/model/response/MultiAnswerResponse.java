package com.incesoft.ai.commonsapi.service.model.response;

import java.util.List;

import com.incesoft.ai.commonsapi.service.model.Answer;
import com.incesoft.ai.commonsapi.service.model.AskResponse;

public class MultiAnswerResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;
	
	private String nodeId;
	private String moduleId;
	private List<Answer> answers;
	
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public List<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}
}
