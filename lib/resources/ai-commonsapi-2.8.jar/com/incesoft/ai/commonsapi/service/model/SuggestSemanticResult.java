package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.List;

public class SuggestSemanticResult implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private WordInfo[] segmentResult;
	private String trace;
	private List<SuggestedSemantic> semantics;
	
	public WordInfo[] getSegmentResult() {
		return segmentResult;
	}
	public void setSegmentResult(WordInfo[] segmentResult) {
		this.segmentResult = segmentResult;
	}
	public List<SuggestedSemantic> getSemantics() {
		return semantics;
	}
	public void setSemantics(List<SuggestedSemantic> semantics) {
		this.semantics = semantics;
	}
	public String getTrace() {
		return trace;
	}
	public void setTrace(String trace) {
		this.trace = trace;
	}
}
