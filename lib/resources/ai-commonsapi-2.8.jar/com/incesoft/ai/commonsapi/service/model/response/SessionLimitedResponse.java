package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskRequest;
import com.incesoft.ai.commonsapi.service.model.AskResponse;

public class SessionLimitedResponse extends AskResponse {

	private static final long serialVersionUID = 1L;
	
	private int sessionsLimit;
	private int sessionTimeout;
	private AskRequest currentRequest;
	
	

	public SessionLimitedResponse(int sessionsLimit, int sessionTimeout, AskRequest currentRequest) {
		this.sessionsLimit = sessionsLimit;
		this.sessionTimeout = sessionTimeout;
		this.currentRequest = currentRequest;
	}
	
	public int getSessionsLimit() {
		return sessionsLimit;
	}

	public void setSessionsLimit(int sessionsLimit) {
		this.sessionsLimit = sessionsLimit;
	}

	public int getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	public AskRequest getCurrentRequest() {
		return currentRequest;
	}

	public void setCurrentRequest(AskRequest currentRequest) {
		this.currentRequest = currentRequest;
	}
}
