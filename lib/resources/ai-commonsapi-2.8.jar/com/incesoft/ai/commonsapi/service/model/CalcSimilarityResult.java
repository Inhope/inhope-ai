package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;

public class CalcSimilarityResult implements Serializable {

	double similarity;
	WordInfo[] segmentResult1;
	WordInfo[] segmentResult2;
	
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
	public WordInfo[] getSegmentResult1() {
		return segmentResult1;
	}
	public void setSegmentResult1(WordInfo[] segmentResult1) {
		this.segmentResult1 = segmentResult1;
	}
	public WordInfo[] getSegmentResult2() {
		return segmentResult2;
	}
	public void setSegmentResult2(WordInfo[] segmentResult2) {
		this.segmentResult2 = segmentResult2;
	}
	
}
