package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;

public class ExternalModule  implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String moduleId;
	private String nodeId;
	private String nodeAddrs;
	private boolean moduleAwared;
	
	public String getNodeAddrs() {
		return nodeAddrs;
	}
	public void setNodeAddrs(String nodeAddrs) {
		this.nodeAddrs = nodeAddrs;
	}
	
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	public String getNodeId() {
		return nodeId;
	}
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}
	public boolean isModuleAwared() {
		return moduleAwared;
	}
	public void setModuleAwared(boolean moduleAwared) {
		this.moduleAwared = moduleAwared;
	}

}
