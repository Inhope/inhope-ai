package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;
import java.util.Arrays;

public class Licence implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final int EDITION_STANDARD = 0;
	private static final int EDITION_ENTERPRISE = 1;
	
	private long expiredTime;
	private int maxDocCount;
	private int maxModuleCount;
	private int requestsWindow;
	private int requestsLimit;
	private int maxPlatformCount;
	private String[] supportedPlatforms;
	private boolean speechEnabled;
	private String description;
	private int sessionTimeout;
	private int sessionsLimit;
	private int edition = EDITION_STANDARD;
	private String secretkey;
	
	public String getSecretKey() {
		return secretkey;
	}
	public void setSecretKey(String secretKey) {
		this.secretkey = secretKey;
	}
	public int getEdition() {
		return edition;
	}
	public void setEdition(int edition) {
		this.edition = edition;
	}
	public int getSessionTimeout() {
		return sessionTimeout;
	}
	public void setSessionTimeout(int sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}
	public int getSessionsLimit() {
		return sessionsLimit;
	}
	public void setSessionsLimit(int sessionsLimit) {
		this.sessionsLimit = sessionsLimit;
	}
	public long getExpiredTime() {
		return expiredTime;
	}
	public void setExpiredTime(long expiredTime) {
		this.expiredTime = expiredTime;
	}
	public int getMaxDocCount() {
		return maxDocCount;
	}
	public void setMaxDocCount(int maxDocCount) {
		this.maxDocCount = maxDocCount;
	}
	public int getMaxModuleCount() {
		return maxModuleCount;
	}
	public void setMaxModuleCount(int maxModuleCount) {
		this.maxModuleCount = maxModuleCount;
	}
	public int getRequestsWindow() {
		return requestsWindow;
	}
	public void setRequestsWindow(int requestsWindow) {
		this.requestsWindow = requestsWindow;
	}
	public int getRequestsLimit() {
		return requestsLimit;
	}
	public void setRequestsLimit(int requestsLimit) {
		this.requestsLimit = requestsLimit;
	}
	public int getMaxPlatformCount() {
		return maxPlatformCount;
	}
	public void setMaxPlatformCount(int maxPlatformCount) {
		this.maxPlatformCount = maxPlatformCount;
	}
	public String[] getSupportedPlatforms() {
		return supportedPlatforms;
	}
	public void setSupportedPlatforms(String[] supportedPlatforms) {
		this.supportedPlatforms = supportedPlatforms;
	}
	public boolean isSpeechEnabled() {
		return speechEnabled;
	}
	public void setSpeechEnabled(boolean speechEnabled) {
		this.speechEnabled = speechEnabled;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return "Licence [expiredTime=" + expiredTime 
				+ ", maxDocCount=" + maxDocCount 
				+ ", maxModuleCount=" + maxModuleCount
				+ ", requestsWindow=" + requestsWindow
				+ ", requestsLimit=" + requestsLimit
				+ ", sessionTimeout=" + sessionTimeout
				+ ", sessionsLimit=" + sessionsLimit
				+ ", maxPlatformCount=" + maxPlatformCount
				+ ", supportedPlatforms=" + Arrays.toString(supportedPlatforms)
				+ ", speechEnabled=" + speechEnabled
				+ ", description=" + description
				+ ", edition=" + edition
				+ ", secretkey=" + secretkey + "]";
	}
	
	
	
}
