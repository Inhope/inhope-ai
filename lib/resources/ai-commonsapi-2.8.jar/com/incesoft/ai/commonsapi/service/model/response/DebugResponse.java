package com.incesoft.ai.commonsapi.service.model.response;

import com.incesoft.ai.commonsapi.service.model.AskResponse;
import com.incesoft.ai.commonsapi.service.model.Question;


public class DebugResponse extends AskResponse {
	
	private static final long serialVersionUID = 1L;

	private AskResponse response;
	
	private int executeTime = 0;
	private Question[] matchedQuestion;
	private float[] similarities;
	private String trace;
	
	public String getTrace() {
		return trace;
	}
	public void setTrace(String trace) {
		this.trace = trace;
	}
	public AskResponse getResponse() {
		return response;
	}
	public void setResponse(AskResponse response) {
		this.response = response;
	}
	
	public Question[] getMatchedQuestion() {
		return matchedQuestion;
	}
	public void setMatchedQuestion(Question[] matchedQuestion) {
		this.matchedQuestion = matchedQuestion;
	}
	public long getExecuteTime() {
		return executeTime;
	}
	public void setExecuteTime(int executeTime) {
		this.executeTime = executeTime;
	}
	public float[] getSimilarities() {
		return similarities;
	}
	public void setSimilarities(float[] similarities) {
		this.similarities = similarities;
	}
	
	
}
