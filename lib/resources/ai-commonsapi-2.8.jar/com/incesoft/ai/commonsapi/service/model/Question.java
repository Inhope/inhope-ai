package com.incesoft.ai.commonsapi.service.model;

import java.io.Serializable;

/**
 * 对知识库录入的问题
 * 问题的概念准确的定义应该为输入引擎的内容，使用时务虚注意！
 * 比如在情景中机器人输出给用户的问题并不是此类所定义的问题概念。
 * @author Boyce Lee
 */
public class Question implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public static final short TYPE_CLASS_SEMANTIC = -6;
	public static final short TYPE_GROUP_ATTRIBUTE = -5;
	public static final short TYPE_REVERSE_ASK_FAKE = -4;
	public static final short TYPE_REVERSE_ASK = -3;
	public static final short TYPE_CLASS_ATTRIBUTE = -2;
	public static final short TYPE_OBJECT_SEMANTIC = -1;
	
	public static final short TYPE_QUESTION = 0;
	public static final short TYPE_TEMPLATE_POSITIVE = 1;
	public static final short TYPE_TEMPLATE_NEGATIVE = 2;
	public static final short TYPE_COMMAND = 3;
	public static final short TYPE_NODE_ID = 4;
	
	private String id;
	private String content;
	private short type = TYPE_QUESTION;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public short getType() {
		return type;
	}
	public void setType(short type) {
		this.type = type;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Question other = (Question) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Question clone() {
		Question question = new Question();
		question.setContent(content);
		question.setId(id);
		question.setType(type);
		return question;
	}

}
