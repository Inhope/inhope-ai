#!/bin/bash
ulimit -n 102400
ulimit -u 102400

APP_NAME="Knity Server"
APP_HOME=/app/ai-engine-shxg-9288
APP_PID=$APP_HOME/knity.pid

JAVA_HOME=/app/java/jdk1.6.0_27
JAVA_OPTS="-Xmn256M -Xms2000M -Xmx2000M -Xss256k -XX:PermSize=256M -XX:MaxPermSize=512M -XX:MaxDirectMemorySize=256M -server -XX:+UseParallelGC -XX:+UseParallelOldGC -XX:MinHeapFreeRatio=10 -XX:MaxHeapFreeRatio=90 -XX:MaxGCPauseMillis=100 -XX:+UseAdaptiveSizePolicy -Djava.nio.channels.spi.SelectorProvider=sun.nio.ch.EPollSelectorProvider -Dsun.net.inetaddr.ttl=60 -Dfile.encoding=GBK -Duser.language=zh -Dknity.home=$APP_HOME/app -DenhanceStandardResponse=true"

JAVA=$JAVA_HOME/bin/java 

CLASSPATH=$APP_HOME/conf:$APP_HOME/app:/app/AILicense_8.0ent
for file in $APP_HOME/lib/*.jar;
	do CLASSPATH=$CLASSPATH:$file;
done

RUN_CMD="$JAVA $JAVA_OPTS -classpath $CLASSPATH com.incesoft.knity.Knity"

APP_CONSOLE=$APP_HOME/logs/stdout.log

ACTION=$1


##################################################
# Do the action
##################################################
case "$ACTION" in
 start)
        echo "Starting $APP_NAME: "

        if [ -f $APP_PID ]
        then
            echo "Already Running!!"
            exit 1
        fi

        echo "STARTED $APP_NAME `date`" >> $APP_CONSOLE

        nohup sh -c "exec $RUN_CMD >>$APP_CONSOLE 2>&1" >/dev/null &
        echo $! > $APP_PID
        echo "$APP_NAME running pid="`cat $APP_PID`
        ;;

  stop)
        PID=`cat $APP_PID 2>/dev/null`
        echo "Shutting down $APP_NAME: $PID"
        kill $PID 2>/dev/null
        sleep 5
        kill -9 $PID 2>/dev/null
        rm -f $APP_PID
        echo "STOPPED `date`" >>$APP_CONSOLE
        ;;
esac

exit 0

